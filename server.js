var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var path = require('path');

app.use(express.static(__dirname + '/build/default'));

app.listen(port);

console.log('app polymer con fachada NodeJS escuchando en el puerto: ' + port);

app.get('/', function(req, res) {
    res.sendFile("index.html", { root: '.' });
});